package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String start, String end, String category) {
		final int LIMIT_NUM = 1000;

		if ( (start != null) && !("".equals(start)))  {
			start = (start + "00:00:00");
		}else {
			start = ("2020-01-01 00:00:00");
		}

		if ( (end != null) && !("".equals(end))) {
			end = (end + " 23:59:59");

		}else {
			end = ("CURRENT_TIMESTAMP");
		}

		if ( (category != null) && !("".equals(category))) {
			category = ("%"+ category + "%");
		}else {
			category = null;
		}


		Connection connection = null;
		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM,  start , end , category);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int deleteMessageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, deleteMessageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
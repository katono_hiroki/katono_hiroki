package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		String repassword = request.getParameter("repassword");

		User user = getUser(request);
		if (!isValid( request, user, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", user.getAccount());
			request.setAttribute("password", user.getPassword());
			request.setAttribute("repassword", repassword);
			request.setAttribute("name", user.getName());
			request.setAttribute("branchId", user.getBranchId());
			request.setAttribute("departmentId", user.getDepartmentId());
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);
		response.sendRedirect("./management");

	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(HttpServletRequest request,User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String repassword = request.getParameter("repassword");
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if (StringUtils.isEmpty(account) || StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length() ) {
			errorMessages.add("アカウント名は6文字以上で入力してください");
		} else if ( 20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}else if (!account.matches( "^[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		}

		if (StringUtils.isEmpty(password) || StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください");
		}else if (6 > password.length() ) {
			errorMessages.add("パスワードは6文字以上で入力してください");
		}else if ( 20 < password.length()) {
			errorMessages.add("パスワードは20文字以下で入力してください");
		}else if (!password.matches( "^[-/:-@\\[-~0-9a-zA-Z]+$")) {
			errorMessages.add("パスワードは半角文字で入力してください");
		}

		if (!StringUtils.isEmpty(password)) {
			if(StringUtils.isEmpty(repassword)) {
				errorMessages.add("確認用パスワードを入力してください");
			}else if(!password.equals(repassword)) {
				errorMessages.add("パスワードと確認用パスワードが一致していません");
			}
		}

		if (StringUtils.isEmpty(name) || StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		}else if (!StringUtils.isEmpty(name) && (10 < name.length())) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if(branchId == 1 && (departmentId == 3 || departmentId == 4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 2 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 3 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 4 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		User differentUser = new UserService().select(account);

		if(differentUser != null) {
			errorMessages.add("アカウント名が重複しています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}

		return true;
	}
}
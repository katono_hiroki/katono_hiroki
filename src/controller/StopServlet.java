package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User stopUser = getStopUser(request);

		new UserService().stop(stopUser);
		response.sendRedirect("./management");
	}

	private User getStopUser(HttpServletRequest request) throws IOException, ServletException {

		User stopUser = new User();
		stopUser.setIsStopped(Byte.parseByte(request.getParameter("isStopped")));
		stopUser.setId(Integer.parseInt(request.getParameter("userId")));
		return stopUser;
	}

}
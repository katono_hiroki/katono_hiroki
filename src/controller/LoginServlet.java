package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> errorMessages = new ArrayList<String>();

		String account = request.getParameter("account");
		String password = request.getParameter("password");

		User user = new UserService().select(account, password);

		if ((user == null) || (user.getIsStopped() == 1)) {
			errorMessages.add("ログインに失敗しました");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("account", account);
			request.setAttribute("password", password);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}


		HttpSession session = request.getSession();

		session.removeAttribute("errorMessages");
		session.setAttribute("loginUser", user);

		response.sendRedirect("./");
	}
}
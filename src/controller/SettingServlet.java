package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		List<String> errorMessages = new ArrayList<String>();

		String id = request.getParameter("userId");
		if (!id.matches( "[0-9]+$") || StringUtils.isBlank(id)) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		User user = new UserService().select(Integer.parseInt(request.getParameter("userId")));

		if (user == null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		request.setAttribute("loginUser" ,loginUser);
		request.setAttribute("user", user);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();


		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");


		User user = getUser(request);
		if (isValid( request, user, errorMessages)) {
			try {
				new UserService().update(user);
			} catch (NoRowsUpdatedRuntimeException e) {
				errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			}
		}

		if (errorMessages.size() != 0) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
			request.setAttribute("account", account);
			request.setAttribute("password", password);
			request.setAttribute("repassword", repassword);
			request.setAttribute("name", name);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}


		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));

		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));


		return user;

	}

	private boolean isValid(HttpServletRequest request , User user, List<String> errorMessages) {

		String name = user.getName();
		String account = user.getAccount();
		String password = user.getPassword();
		String repassword = request.getParameter("repassword");
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();


		if (StringUtils.isEmpty(account) || StringUtils.isBlank(account)) {
			errorMessages.add("アカウント名を入力してください");
		} else if (6 > account.length()) {
			errorMessages.add("アカウント名は6文字以上で入力してください");
		} else if (20 < account.length()) {
			errorMessages.add("アカウント名は20文字以下で入力してください");
		}else if (!account.matches( "[0-9a-zA-Z]+$")) {
			errorMessages.add("アカウント名は半角英数字で入力してください");
		}

		if (!StringUtils.isEmpty(password) && StringUtils.isBlank(password)) {
			if (6 > password.length() ) {
				errorMessages.add("パスワードは6文字以上で入力してください");
			}else if ( 20 < password.length()) {
				errorMessages.add("パスワードは20文字以下で入力してください");
			}else if (!password.matches( "^[-/:-@\\[-~0-9a-zA-Z]+$")) {
				errorMessages.add("パスワードは半角文字で入力してください");
			}
		}

		if (!StringUtils.isEmpty(password) && StringUtils.isEmpty(repassword)) {
			errorMessages.add("確認用パスワードを入力してください");
		}else if(!password.equals(repassword)) {
			errorMessages.add("パスワードと確認用パスワードが一致していません");
		}

		if (StringUtils.isEmpty(name) || StringUtils.isBlank(name)) {
			errorMessages.add("名前を入力してください");
		}else if (!StringUtils.isEmpty(name) && (10 < name.length())) {
			errorMessages.add("名前は10文字以下で入力してください");
		}

		if(branchId == 1 && (departmentId == 3 || departmentId == 4)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 2 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 3 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		if(branchId == 4 && (departmentId == 1 || departmentId == 2)) {
			errorMessages.add("支社と部署の組み合わせが間違っています");
		}

		User differentUser = new UserService().select(account);
		String differentAccount = request.getParameter("diaccount");;

		if((differentUser != null) && (!differentAccount.equals(account))) {
			errorMessages.add("アカウント名が重複しています");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}

}
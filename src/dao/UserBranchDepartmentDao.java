package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    users.id as id, ");
			sql.append("    users.account as account, ");
			sql.append("    users.name as name, ");
			sql.append("    users.branch_id as branch_id, ");
			sql.append("    users.department_id as department_id, ");
			sql.append("    users.is_stopped as is_stopped, ");
			sql.append("    users.created_date as created_date, ");
			sql.append("    branches.name as branch_name, ");
			sql.append("    departments.name as department_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON branches.id = users.branch_id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON departments.id = users.department_id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> users = toUsers(rs);
			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUsers(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> users = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();
				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setBranchName(rs.getString("branch_name"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setDepartmentName(rs.getString("department_name"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setCreatedDate(rs.getTimestamp("created_date"));

				users.add(user);
			}
			return users;
		} finally {
			close(rs);
		}
	}
}
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<form action="message" method="post"><br />
				<a href="./">ホーム</a>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }" />
					</c:forEach>
					</ul>
			</div>
		</c:if>
				<p>
				件名：　<input type="text" name="title" size="40" value = "${ title }">
				</p>
				<p>
				カテゴリ：<input type="text" name="category" size="40"  value = "${ category }">
				</p>
				投稿内容：　
				<textarea name="text" cols="30" rows="5" class="text-box">${ text }</textarea>
				<br /><br/>
				<input type="submit" value="投稿"><br/><br/>
			</form>
		</div>
		<div class="copyright"> Copyright(c)Katono Hiroki</div>
	</body>
</html>
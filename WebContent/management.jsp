<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" />
						</c:forEach>
 					</ul>
				</div>
			</c:if>

				<a href="./">ホーム</a>            <a href="./signup">ユーザー新規登録</a><br />
				<div class="users">
					<c:forEach items="${users}" var="user">
					<div class="user">
						<div class="account">アカウント：<c:out value="${user.account}" /></div>
						<div class="name">名前：<c:out value="${user.name}" /></div>
						<div class="branch_id">支社：<c:out value="${user.branchName}" /></div>
						<div class="department_id">部署：<c:out value="${user.departmentName}" /></div>
						<div class="is_stopped">アカウント復活停止状態:
						<c:if test="${ user.isStopped == 0 }">
						稼働中</c:if>
						<c:if test="${ user.isStopped == 1 }">
						停止中</c:if>
						</div>

						 <form action="setting">
							<input type="hidden" name="userId" value="${user.id}">
							<input type="submit" value="編集">
						</form>
						<c:if test = "${ loginUser.id != user.id}" >
						 <c:if test="${ user.isStopped == 0 }">
							<form action="stop" method="post">
								<input type="hidden" name="userId" value="${user.id}">
								<input type="hidden" name="isStopped" value="1">
								<input type="submit" value="停止"  onclick="stopPost(this);return false;">
							</form>
							<script>
							function stopPost(e) {
								"use strict";
								if (window.confirm('本当に停止しますか？')) {
									document.stop.submit();
									window.alert('停止しました');
									return true;
								} else {
									window.alert('キャンセルされました');
									return false;
								}
							}
						</script>
						</c:if>
						<c:if test="${ user.isStopped == 1 }">
							<form action="stop" method="post">
								<input type="hidden" name="userId" value="${user.id}">
								<input type="hidden" name="isStopped" value="0">
								<input type="submit" value="復活"  onclick="rivivalPost(this);return false;">
							</form>
							<script>
							function rivivalPost(e) {
								"use strict";
								if (window.confirm('本当に復活しますか？')) {
									document.rivival.submit();
									window.alert('復活しました');
									return true;
								} else {
									window.alert('キャンセルされました');
									return false;
								}
							}
						</script>
						</c:if>
						</c:if>
					</div><br/><br/>
					</c:forEach>
				</div>
			</div>
		<div class="copyright">Copyright(c)Katono Hiroki</div>

	</body>
</html>
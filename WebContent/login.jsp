<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>

		<div class="main-contents">


			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" />
						</c:forEach>
					</ul>
				</div>
			</c:if>


			<center>
			<div class="login">
			<form action="login" method="post"><br />
				<label for="account">アカウント</label><br />
				<input name="account" id="account"value="${account}"/> <br /><br />

				<label for="password">パスワード</label><br />
				<input name="password" type="password" id="password" value="${password}"/> <br /><br /><br />

				<input type="submit" value="ログイン" /> <br /><br />
				<a href="./">戻る</a>
			</form>
			</div>
			</center>

			<div class="copyright"> Copyright(c)Katono Hiroki</div>
		</div>
	</body>
</html>
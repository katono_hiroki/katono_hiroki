<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents" >



			<form action="signup" method="post"><br />
				<a href="./management">ユーザー管理</a> <br />

<c:if test="${ not empty errorMessages }">
				<div class="errorMessages" >
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" />
						</c:forEach>
 					</ul>
				</div>
			</c:if>
				<label for="account">アカウント</label>
				<input name="account" id="account" value = "${account}" /> <br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" value = "${password}"  /> <br />
				<label for="repassword">確認用パスワード</label>
				<input name="repassword" type="password" id="repassword" value = "${repassword}" /> <br />

				<label for="name">名前</label>
				<input name="name" id="name" value = "${name}"/> <br />

				<p>
				支社
				<select size = "1" name="branchId" id = "branchId" >
					<c:forEach items="${branches}" var="branch">
					<c:if test = "${branch.id == branchId }">
						<option value="${branch.id }" selected>${branch.name}</option>
					</c:if>
					<c:if test = "${branch.id != branchId }">
						<option value="${branch.id }" >${branch.name}</option>
					</c:if>
					</c:forEach>
				</select><br />
				</p>
				<p>
				部署
				<select size = "1" name="departmentId" id = "departmentId">
					<c:forEach items="${departments}" var="department">
					<c:if test = "${department.id == departmentId }">
						<option value="${department.id }" selected>${department.name}</option>
					</c:if>
					<c:if test = "${department.id != departmentId }">
						<option value="${department.id }" >${department.name}</option>
					</c:if>
					</c:forEach>
				</select><br />
				</p>

				<input type="submit" value="登録" /> <br />

			</form>

			<div class="copyright">Copyright(c)Katono Hiroki</div>
		</div>
	</body>
</html>
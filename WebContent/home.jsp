<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<div class="header">

				<a href="./">ホーム</a>
				<a href="message">新規投稿</a>

				<c:if test = "${ loginUser.departmentId == 1}">
				<a href="management">ユーザー管理</a>
				</c:if>

				<a href="logout">ログアウト</a>


		</div>

		<form action="./">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${ errorMessages }" var="errorMessage">
						<li><c:out value="${ errorMessage }" />
					</c:forEach>
				</ul>
				<c:remove var = "errorMessages" scope ="session" />
			</div>
		</c:if>
			<br />
		 <label for="date">日付指定</label> <input type="date" id="start"name="start" value="${start}"></input>　
			～　<input type="date" id="end" name="end" value="${end}"></input><br/><br/>
			<label for="">カテゴリ検索</label>
			<input name="category" id="category" value="${category}" /> <input type="submit" value="絞り込み"><br/><br/>
		</form>
		------------------------------------------------------------------------------------------------------------------------------<br /> <br />


		<script>
			function deletePost(e) {
				"use strict";
				if (window.confirm('本当に削除しますか？')) {
					document.deleteMessage.submit();
					window.alert('削除しました');
					return true;
				} else {
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="name">
						<span class="name">投稿者：<c:out value="${message.name}" /></span>
					</div>
					<div class="title">
						件名：
						<c:out value="${message.title}" />
					</div>
					<div class="category">
						カテゴリ：
						<c:out value="${message.category}" />
					</div>
					<div class="text" style="white-space: pre-line;">
						本文：<br />
						<c:out value="${message.text}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>

					<c:if test="${ loginUser.id == message.userId }">
						<form action="deleteMessage" method="post">
							<input type="hidden" name="deleteMessageId" value="${message.id}">
							<input type="submit" value="削除" onclick="deletePost(this);return false;" />
						</form>

					</c:if>
					</div>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
								<c:if test="${ message.id == comment.messageId }">
									<div class="name">
										<span class="name">コメント作成者：<c:out
												value="${comment.name}" /></span>
									</div>
									<div class="text" style="white-space: pre-line;">
										コメント：<br />
										<c:out value="${comment.text}" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.createdDate}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>

									<c:if test="${ loginUser.id == comment.userId }">
										<form action="deleteComment" method="post">
											<input type="hidden" name="deleteCommentId"
												value="${comment.id}"> <input type="submit"
												value="削除" onclick="deletePost(this);return false;" />
										</form>
									</c:if>
								</c:if>
							</div>
						</c:forEach>
					</div>
					<form action="comment" method="post">

						<p>コメント入力欄</p>
						<textarea name="comment" cols="100" rows="5" class="comment-box" ></textarea>
						<br /> <input type="hidden" name="messageId"
							value="${message.id}"> <input type="submit"
							value="コメント投稿">
					</form>
			</c:forEach>
		</div>
	</div>
	<div class="copyright">Copyright(c)Katono Hiroki</div>
</body>
</html>